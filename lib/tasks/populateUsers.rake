namespace :users do
    task :populate => :environment do
      
      if User.last
        start = User.last.id + 1
        finish = User.last.id + 21        
      else
        start = 1
        finish = 20
      end
      
      for i in start..finish
        firstname = Faker::Name.first_name
        lastname = Faker::Name.last_name        
        u = User.create(
          :name => "#{firstname} #{lastname}", 
          :name_first => "#{firstname}", 
          :name_last => "#{lastname}", 
          :name_nick => "#{Faker::Company::catch_phrase}",
          :email_address => "#{Faker::Internet.free_email("#{firstname} #{lastname}")}",
          :hometown => "#{Faker::Address::city}", 
          :tagline => MOST_LIKELY_TO[rand(MOST_LIKELY_TO.length)], 
          :attendance => rand((1..7))
        )
        u.entries.create(:event => Event.first, :status => ENTRY_STATUS_REGISTERED, :created_at => Time.now + i.minute)
      end
    end
    
    task :destroy_all => :environment do 
      User.destroy_all
      Entry.destroy_all
      Image.destroy_all
    end
    
    task :create_admins => :environment do
      AdminUser.create!(:email => 'photo@roundhouseagency.com', :password => '4u2shoot', :password_confirmation => '4u2shoot', :role => 'photographer')
      AdminUser.create!(:email => 'edit@roundhouseagency.com', :password => '4u2edit', :password_confirmation => '4u2edit', :role => 'editor')
      AdminUser.create!(:email => 'admin@roundhouseagency.com', :password => '4u2admin', :password_confirmation => '4u2admin', :role => 'admin')
    end
    
    task :populate_cloud => :environment do
      image = CloudImage.first
      (1..2000).each do |i|
        firstname = Faker::Name.first_name
        lastname = Faker::Name.last_name
        u = CloudUser.create(
          name_first: firstname,
          name_last: lastname,
          email_address: Faker::Internet.free_email("#{firstname} #{lastname}"),
          hometown: Faker::Address::city,
          name_nick: Faker::Company::catch_phrase,
          tagline: Faker::Company::bs,
          attendance: [(1..7)].sample
        )
        pp "--------"
        pp u
        
        u.entries.create(
          status: ENTRY_STATUS_COMPLETED,
          selected_image: image
        )
      end
    end
    
  end