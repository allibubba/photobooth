require 'pathname'
require 'fileutils'
require 'open-uri'
require 'cloudfiles'
require 'mime/types'
require 'pp'
namespace :lifestyle do
    task :process => :environment do
      #move to CDN
      puts "-------------------------------------------------------------------------------------------------"
      puts "  Create new Lifestyle records, and move /tmp/lifestyle/"
      puts "  images to CDN?"
      puts ""
      puts "  [yes/no]"
      puts "-------------------------------------------------------------------------------------------------"
      
      cdn_answer = $stdin.gets.chomp

      if cdn_answer.downcase == "yes"
        puts "continuing..............."
        # cloud
        cloudfiles = CloudFiles::Connection.new(:username => RACKSPACE_USER, :api_key => RACKSPACE_KEY)
        sasquatch_container = cloudfiles.container(RACKSPACE_BUCKET)
        lifestyle_directory = "#{Rails.root}/tmp/lifestyle"
        lifestyle_files = Dir["#{Rails.root}/tmp/lifestyle/*.jpg"]
        lifestyle_files.each do |lifestyle_file|
          file = File.open(lifestyle_file)
          content_types = MIME::Types.type_for(file.path)
          content_type = content_types.first.content_type if content_types && content_types.size > 0
          meta = {'Content-Type' => content_type}
          remote_item = "lifestyle/#{Pathname.new(lifestyle_file).basename}"
          object = sasquatch_container.create_object(remote_item,true)
          object.write(file.read,meta)

          pp "uploded: #{object}"
          
          local_image = Lifestyle.create(filename: object.public_url)
          local_image.sync
          
          File.delete(lifestyle_file)
        end
        puts "Done, grab a beer"
      else
        puts "aborted..."
      end
    end
  end
