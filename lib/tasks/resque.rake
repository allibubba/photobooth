require "resque/tasks"
require 'resque_scheduler/tasks'
namespace :resque do
  task :setup => :environment do

  end
  desc "kill all workers (using -QUIT), god will take care of them"
  task :stop_workers => :environment do

    pids = Array.new
    Resque.workers.each do |worker|
      pids << worker.to_s.split(/:/).second
    end
    if pids.size > 0
      system("kill -9 #{pids.join(' ')}")
    end
  end
end
