module ApplicationHelper
  require 'timeout'
  require 'socket'
  def ping(host)
    begin
      Timeout.timeout(5) do 
        s = TCPSocket.new(host, 'echo')
        s.close
        return true
      end
    rescue Errno::ECONNREFUSED
      return true
    rescue Timeout::Error
      return false
    end
  end
  
end
