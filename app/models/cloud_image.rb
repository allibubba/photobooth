require 'pathname'
require 'fileutils'
require 'open-uri'
require 'cloudfiles'
require 'mime/types'



class CloudImage < Image
  attr_accessible  :id,
                  :remote_path,
                  :entry_id,
                  :created_at,
                  :updated_at,
                  :local_file,
                  :file_file_name,
                  :file_content_type,
                  :file_file_size,
                  :file_updated_at,
                  :thumb,
                  :medium,
                  :large,
                  :original,
                  :type  
  establish_connection "cloud_#{Rails.env}"
  before_save :upload_image_versions
  
  def upload_image_versions
    cloudfiles = CloudFiles::Connection.new(:username => RACKSPACE_USER, :api_key => RACKSPACE_KEY)
    sasquatch_container = cloudfiles.container(RACKSPACE_BUCKET)
    ['thumb','large','original'].each do |version|
      relative_path = read_attribute(version)
      full_path = "#{Rails.root}/public#{relative_path}"      
      file = File.open(full_path) if File.exists?(full_path)
      pp "Uploading .... #{relative_path}"
      if file
        content_types = MIME::Types.type_for(file.path)
        content_type = content_types.first.content_type if content_types && content_types.size > 0
        meta = {'Content-Type' => content_type}
        object = sasquatch_container.create_object(relative_path.gsub(/^\//,""),true)
        object.write(file.read,meta)
        write_attribute(version,object.public_url)
      else 
        false
      end
    end
  end
  
end