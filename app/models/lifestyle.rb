class Lifestyle < ActiveRecord::Base  
  attr_accessible :caption, :filename
  validates_presence_of :filename
  def sync
    Lifestyle.connect_to_cloud
    data = {
      filename: self.filename,
      caption: self.caption
    }    
    uploaded = Lifestyle.create(data)
    Lifestyle.connect_to_local
  end

  def self.connect_to_cloud
    ActiveRecord::Base::clear_active_connections!
    ActiveRecord::Base::establish_connection "cloud_#{Rails.env}"    
  end

  def self.connect_to_local
    ActiveRecord::Base::clear_active_connections!
    configurations = ActiveRecord::Base::configurations
    ActiveRecord::Base::establish_connection configurations[Rails.env.to_sym]
  end
  
end
