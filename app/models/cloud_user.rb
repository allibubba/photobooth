class CloudUser < User
  attr_accessible :name,
                  :name_first,
                  :name_last,
                  :name_nick,
                  :hometown,
                  :email_address,
                  :attendance,
                  :tagline,
                  :created_at,
                  :updated_at
                  
  establish_connection "cloud_#{Rails.env}"
  has_many :entries, :class_name => "CloudEntry", :foreign_key => "user_id"
end