class Entry < ActiveRecord::Base
  require "fileutils" #required for complete_photography - Used to move files to the correct directory

  belongs_to :user
  belongs_to :event
  has_many :images
  belongs_to :selected_image, :class_name => "Image"
  belongs_to :photographer, :class_name => "AdminUser"
  belongs_to :editor, :class_name => "AdminUser"
  
  scope :photography_queue, where("status = ? OR status = ? or status = ?", ENTRY_STATUS_REGISTERED, ENTRY_STATUS_PHOTOGRAPHY_ACTIVE,ENTRY_STATUS_PHOTOGRAPHY_INACTIVE).order("created_at ASC")
  scope :editing_queue, where("status = ? OR status = ? OR status = ?", ENTRY_STATUS_PHOTOGRAPHY_COMPLETE, ENTRY_STATUS_EDITING_ACTIVE, ENTRY_STATUS_EDITING_INACTIVE).order("created_at ASC")
  scope :flagged, where('status = ?', ENTRY_STATUS_FLAGGED_FOR_MODERATION).order("updated_at ASC")
  scope :deleted, where('status = ?', ENTRY_STATUS_DELETED).order("updated_at ASC")
  scope :completed, where('status = ?', ENTRY_STATUS_COMPLETED).order("updated_at ASC")

  # attr_accessor :entry_directory
  # after_save :create_entry_path
  
  
  def start_photography
    self.status = ENTRY_STATUS_PHOTOGRAPHY_ACTIVE
    save = self.save
    if save
      PrivatePub.publish_to("/queue/photo", {:entry_id => self.id,:action => 'start'})
    end   
    return save
  end
  
  def stop_photography
    self.status = ENTRY_STATUS_PHOTOGRAPHY_INACTIVE
    save = self.save
    if save
      PrivatePub.publish_to("/queue/photo", {:entry_id => self.id,:action => 'stop'})
    end
    return save
  end
  
  def complete_photography
    source_path =  File.expand_path PATH_IMAGES_RAW
    files = Dir.glob("#{source_path}/*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}")
    if files.count == 0
      return false
    else
      self.status = ENTRY_STATUS_PHOTOGRAPHY_COMPLETE
      save = self.save
      if save
        PrivatePub.publish_to("/queue/photo", {:entry_id => self.id,:action => 'complete'})
        Dir.foreach(source_path) do |item|
          next if item == '.' or item == '..' or item.starts_with?('.')
          filename = File.basename(item).to_s
          self.images.create(:tmp_path => source_path+ "/" +filename)
        end
        PrivatePub.publish_to("/queue/edit", {:entry_id => self.id,:action => 'create', :user => self.user, :entry => self, :images => self.images})
      end
      return save
    end
  end

  def start_editing
    self.status = ENTRY_STATUS_EDITING_ACTIVE
    save = self.save
    if save
      PrivatePub.publish_to("/queue/edit", {:entry_id => self.id,:action => 'start'})      
    end   
    return save
  end
  
  def stop_editing
    self.status = ENTRY_STATUS_EDITING_INACTIVE
    save = self.save
    if save
      PrivatePub.publish_to("/queue/edit", {:entry_id => self.id,:action => 'stop'})      
    end
    return save
  end
  
  def complete_editing
    if self.selected_image_id
      self.status = ENTRY_STATUS_COMPLETED    
      save = self.save
      if save
        PrivatePub.publish_to("/queue/edit", {:entry_id => self.id,:action => 'complete'})
        data = self.attributes
        data[:selected_image] = self.selected_image.attributes
        data[:user] = self.user.attributes
        PrivatePub.publish_to("/entry/complete", data)
      end
      return save
    else
      false
    end
  end

  def flag
    self.status = ENTRY_STATUS_FLAGGED_FOR_MODERATION
    self.save
  end

  def delete
    PrivatePub.publish_to("/entry/delete", {:entry_id => self.id,:action => 'delete'})
    self.status = ENTRY_STATUS_DELETED
    self.save
  end  
end
