class CloudEntry < Entry
  establish_connection "cloud_#{Rails.env}"  
  belongs_to :user, :class_name => "CloudUser", :foreign_key => "user_id"
  has_many :images, :class_name => "CloudImage"
  belongs_to :selected_image, :class_name => "CloudImage", :foreign_key => "selected_image_id"
end