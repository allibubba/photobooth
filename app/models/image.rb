class Image < ActiveRecord::Base
  belongs_to :entry

  after_initialize :set_local_file
  before_create :move_image
  before_save :update_image_versions
  attr_accessor :tmp_path
  attr_accessible :file, :remote_path, :tmp_path, :entry_id, :thumb, :medium, :large, :original
  
  has_attached_file :file,
    :url => "/uploads/:id/:style/:basename.:extension",
    :path => ":rails_root/public/uploads/:id/:style/:basename.:extension",
    :styles => {
      :thumb => "158x196#",
      :large => {
        :geometry       => "532x632#",
        :watermark_path => "#{Rails.root}/public/watermark.png",
        :position => "SouthWest"
      }
     },
    :default_style => :thumb,
    :convert_options => {
      :thumb => "-background black -gravity NorthWest -extent 158x196",
      :large => "-background black -gravity NorthWest -extent 532x632"
    },
    :processors => [:thumbnail,:watermark]
  
  def move_image
    if @tmp_path
      self.file = File.open(@tmp_path)
      File.delete(@tmp_path)
    end
  end
  
  def original
    self.file.url(:original)
  end

  def large
    self.file.url(:large)
  end

  def medium
    self.file.url(:medium)
  end

  def thumb
    self.file.url(:thumb)
  end

  def set_local_file
    self.local_file = self.file.url
  end
  def update_image_versions
      self.thumb = self.file.url(:thumb,false)
      self.medium = self.file.url(:medium,false)
      self.large = self.file.url(:large,false)
      self.original = self.file.url(:original,false)
  end
end
