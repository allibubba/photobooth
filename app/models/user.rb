class User < ActiveRecord::Base  
  has_many :entries
  has_many :events, :through => :entries
  validates_presence_of :name_first
  validates_presence_of :name_last
  validates_presence_of :hometown
  validates_presence_of :name_nick
  validates :email_address, :presence => {:message => "You must supply and email address."}, :uniqueness => {:case_sensitive => false, :message => "Email address is already being used.", :scope => :type}, :email_format => {:message => 'Must give a valid email address.'}

  attr_accessor :terms
  validates :terms, acceptance: true
end
