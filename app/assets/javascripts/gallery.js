var shown = [];
var rows = 3;
var columns = 6;
var padding = 20;
var timeout = 1500;
var height,
    width,
    total,
    container;
$(function(){
  //init
  container = $('#container');
  height = Math.floor(container.height()/rows)-10;
  width = Math.floor(container.width()/columns)-10;
  total = rows*columns;
  var image;
  var elem;
  for(var i=0;i<total;i++) {
    if(typeof images != "undefined" && images[i]) {
      elem = $('<div>').addClass('image').css({'width':width,'height':height});
      image = $('<img>').attr('src', images[i].selected_image.large).css({'max-width':width-padding,'max-height':height-padding});
      elem.append(image);
      container.append(elem);
      shown.push(i);
    } else {
      break;
    }
  }

  $(window).resize(size);

  var t,
      last_position,
      transitions = [{name:'linewipe',time:1000}];
  function timedSwitch() {
    var random_position = getRand(total-1);
    while(random_position == last_position) {random_position = getRand(total-1);}
    if(typeof images != "undefined" && images.length == shown.length) {shown = [];}
    var random_image = getRandImage();
    if (!random_image) {
      return false
    }
    var container = $('.image').eq(random_position);
    var transition_index = getRand(transitions.length-1);
    container.addClass(transitions[transition_index].name);
    var old_img = container.children('img');
    old_img.addClass('outgoing');
    var new_img = $('<img>').attr('src', random_image.selected_image.large).css({'max-width':width-padding,'max-height':height-padding}).addClass('incoming');
    new_img.load(function() {
      container.addClass('active');
      setTimeout(function(){
        container.removeClass('active');
        new_img.removeClass('incoming');
        old_img.remove();
      },transitions[transition_index].time);
      // old_img.fadeOut(1000, function() {
      //   $(this).remove();
      // });
      // new_img.fadeIn(1000);
      t = setTimeout(timedSwitch, timeout);
    }).error(function() {
      t = setTimeout(timedSwitch, timeout);
    });
    container.append(new_img);

    last_position = random_position;
  }
  t = setTimeout(timedSwitch, timeout);
  console.log(PrivatePub)
  PrivatePub.subscribe("/entry/complete",add_completed_entry)
});

function add_completed_entry (entry) {
  images.push(entry)
}

function getRand(_top) {
  var rand = Math.random()*_top;
  return Math.round(rand);
}

function getRandImage() {
  if (typeof images == "undefined") {
    return false
  }
  
  var index = Math.round(Math.random()*(images.length-1));
  while($.inArray(index,shown) >= 0) {index = Math.round(Math.random()*(images.length-1));}
  shown.push(index);
  // log(index, shown);
  return images[index];
}

function size() {
  height = Math.floor(container.height()/rows)-10;
  width = Math.floor(container.width()/columns)-10;
  $('.image').css({'width':width,'height':height});
  $('.image img').css({'max-width':width-padding,'max-height':height-padding});
}

//window log plugin to disregard no log errors
window.log = function f(){ log.history = log.history || []; log.history.push(arguments); if(this.console) { var args = arguments, newarr; args.callee = args.callee.caller; newarr = [].slice.call(args); if (typeof console.log === 'object') log.apply.call(console.log, console, newarr); else console.log.apply(console, newarr);}};
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());