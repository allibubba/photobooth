class @QueueManager
  constructor: (options)->
    console.log("QueueManager Created", @)
    {@pusher} = options
    @queues = {}
  addQueue: (queue) =>
    @queues[queue.name] = queue
    if @pusher
      @pusher.subscribe("/queue/#{queue.name}",queue.notifcation_receieved)
      @pusher.subscribe("/entry/delete",queue.delete_entry_notification)
    queue
  removeQueue: (name) =>
    @queues[name] = null if @queues[name]

class @Queue
  constructor: (name,target,options)->
    {@entry_selector,@onEntryStart,@onEntryStop,@onEntryComplete,@onEntryCompleteError,@onEntryLock,@onEntryUnlock,@createEntryHtml} = options
    @name = name
    @element = $(target)
    @entries = {}
    @active_entry = null
    if @entry_selector
      for el in @element.find(@entry_selector)
        @addEntry(el)
        if $(el).hasClass('active owner')
          @active_entry = $(el).find('a[data-action|="start"]').trigger('click')
  addEntry: (el) =>
    entry = new Entry(el,{
      onEntryLock: @onEntryLock,
      onEntryUnlock: @onEntryUnlock,
      onEntryStart: @onEntryStart,
      onEntryStop: @onEntryStop,
      onEntryComplete: @onEntryComplete,
      onEntryCompleteError: @onEntryCompleteError,
    })
    @entries[entry.id] = entry
  removeEntry: (entry_id) =>
    @entries[entry_id].delete() if @entries[entry_id]
    @entries[entry_id] = null
  delete_entry_notification: (notification,channel) =>
    @removeEntry(notification.entry_id)
  notifcation_receieved: (notification,channel) =>
    if notification.action is 'create'
      el = @createEntryHtml(notification,@element)
      @addEntry(el)

    # GET THE ENTRY FOR THE NOTIFICATION
    entry = @entries[notification.entry_id]
    if notification.action is "start"
      @active_entry = entry
    else if notification.action is "stop"
      @active_entry = null
    else if notification.action is 'complete'
      @removeEntry(entry.id)
    entry.messaged(notification)

class @Entry
  constructor: (target,options) ->
    @element = $(target)
    @id = @element.data('entry-id')
    {@onEntryStart,@onEntryStop,@onEntryComplete,@onEntryLock,@onEntryUnlock,@onEntryCompleteError} = options
    @element.find('a[data-action|="start"]').bind('ajax:before',@lock)
    @element.find('a[data-action|="start"]').bind('ajax:success',@start)
    @element.find('a[data-action|="stop"]').bind('ajax:success',@stop)
    @element.find('a[data-action|="complete"]').bind('ajax:success',@complete)
    # @element.find('a[data-action|="delete"]').bind('ajax:success',@delete)
  delete: () =>
    @onEntryDelete(@element) if @onEntryDelete
    @element.remove()
  lock: () =>
    console.log "LOCK ... "
    @onEntryLock(@element) if @onEntryLock
  unlock: () =>
    console.log "UNLOCK ... "
    @onEntryUnlock(@element) if @onEntryUnlock
  start: (el,data) =>
    console.log "START .... "
    @onEntryStart(@element,data) if @onEntryStart
  stop: (el,data) =>
    console.log "STOP .... "
    @onEntryStop(@element,data) if @onEntryStop
    @unlock()
  complete: (el,data) =>
    if data
      @onEntryComplete(@element,data) if @onEntryComplete
      @delete()
    else
      @onEntryCompleteError(@element,data) if @onEntryCompleteError
  messaged: (message) =>
    if message.action is "start"
      @element.addClass('active')
    else if message.action is "stop"
      @element.removeClass('active')
