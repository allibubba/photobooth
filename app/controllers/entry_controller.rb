class EntryController < ApplicationController
  require 'pp'
  
  def photography
    # current_admin_user    
    @entries = Entry.photography_queue
    @has_active = @entries.index{|entry| entry.status == ENTRY_STATUS_PHOTOGRAPHY_ACTIVE}
    @admin_user = current_admin_user    
    
    entry = @entries.first || Entry.new(status: ENTRY_STATUS_PHOTOGRAPHY_ACTIVE)
    
    if can? :manage, entry
      render :action => "photography_queue"
    else
      redirect_to root_path, :notice => "You have to be a photographer to view the queue."
    end

  end

  def editing
    @entries = Entry.editing_queue
    @has_active = @entries.index{|entry| entry.status == ENTRY_STATUS_EDITING_ACTIVE}
    @admin_user = current_admin_user  
    
    entry = @entries.first || Entry.new(status: ENTRY_STATUS_PHOTOGRAPHY_COMPLETE)
    
    if can? :manage, entry
      respond_to do |format|
        format.html {render :action => "editing_queue"}
        format.xml { render :xml => @entries}
        format.json { render :json => @entries.to_json(:include => [:user,:images])}
      end        
    else
      redirect_to root_path, :notice => "You must be an editor to view the queue."
    end
  end

  def open_image
    if params[:id]
      image = Image.find(params[:id])
      image.open
      respond_to do |format|
        format.xml { render :xml => image}
        format.json { render :json => image}
      end            
    end
  end

  def completed
    @completed_entries = Entry.completed
    render :layout => 'gallery'
  end

  def last
    if Entry.editing_queue.count > 0
      @last_entry = Entry.editing_queue.last.images.last
      pp @last_entry
    else
      @last_entry = Entry.completed.last.images.last
    end
    render :layout => 'gallery'
  end

  # /* ***********************************************
  # UTILITY METHODS FOR ALLOWING STATUS SWAPPING AND RESPONDING TO THE CHANGE OF STATUS.
  # THIS NEEDS REFACTORING -- A LOT OF DUPLICATE CODE.
  # *********************************************** */
  def photography_start    
    if params[:id]
      source_path =  File.expand_path PATH_IMAGES_RAW      
      Dir.foreach(source_path) do |item|
        next if item == '.' or item == '..'
        File.delete(source_path +"/"+ item)
      end
      
      
      entry = Entry.find(params[:id])
      if can? :manage, entry
        entry.photographer = current_admin_user
        if entry.start_photography    
          respond_to do |format|
            format.xml { render :xml => entry}
            format.json { render :json => entry}
          end
        else
          respond_to do |format|
            format.xml { render :xml => nil}
            format.json { render :json => nil}
          end
        end      
      else
          respond_to do |format|
            format.xml { render :xml => nil}
            format.json { render :json => nil}
          end
      end
    end
  end
  
  def photography_stop    
    if params[:id]
      entry = Entry.find(params[:id])
      if can? :manage, entry
        entry.photographer = nil
        if entry.stop_photography    
          respond_to do |format|
            format.xml { render :xml => entry}
            format.json { render :json => entry}
          end
        else
          respond_to do |format|
            format.xml { render :xml => nil}
            format.json { render :json => nil}
          end
        end      
      else
          respond_to do |format|
            format.xml { render :xml => nil}
            format.json { render :json => nil}
          end
      end
    end
  end
  
  def photography_completed
    if params[:id]
      entry = Entry.find(params[:id])
      if can? :manage, entry
        entry.photographer = current_admin_user
        if entry.complete_photography    
          respond_to do |format|
            format.xml { render :xml => entry}
            format.json { render :json => entry}
          end
        else
          respond_to do |format|
            format.xml { render :xml => false}
            format.json { render :json => false}
          end
        end      
      else
          respond_to do |format|
            format.xml { render :xml => false}
            format.json { render :json => false}
          end
      end
    end
  end
    
  def editing_start
    if params[:id]
      entry = Entry.find(params[:id])
      
      data = {
        :entry => entry,
        :images => entry.images,
        :user => entry.user
      }
      if can? :manage, entry
        entry.editor = current_admin_user
        if entry.start_editing
          respond_to do |format|
            format.xml { render :xml => data}
            format.json { render :json => data}
          end
        else
          respond_to do |format|
            format.xml { render :xml => nil}
            format.json { render :json => nil}
          end
        end
      else
        respond_to do |format|
          format.xml { render :xml => nil}
          format.json { render :json => nil}
        end
      end      
    end
  end
  
  def editing_stop
    if params[:id]
      entry = Entry.find(params[:id])
      if can? :manage, entry
        entry.editor = nil
        if entry.stop_editing
          respond_to do |format|
            format.xml { render :xml => entry}
            format.json { render :json => entry}
          end
        else
          respond_to do |format|
            format.xml { render :xml => nil}
            format.json { render :json => nil}
          end
        end
      else
        respond_to do |format|
          format.xml { render :xml => nil}
          format.json { render :json => nil}
        end
      end      
    end
  end
  
  def editing_completed
    if params[:id]
      entry = Entry.find(params[:id])
      if can? :manage, entry
        if entry.complete_editing
          Resque.enqueue(EntryUploader,entry.id)
          respond_to do |format|
            format.xml { render :xml => entry}
            format.json { render :json => entry}
          end
        else
          respond_to do |format|
            format.xml { render :xml => false}
            format.json { render :json => false}
          end
        end
      else
        respond_to do |format|
          format.xml { render :xml => false}
          format.json { render :json => false}
        end
      end      
    end
  end

  def flag
    if params[:id]
      entry = Entry.find(params[:id])
      if can? :manage, entry      
        if entry.flag
          respond_to do |format|
            format.xml { render :xml => entry}
            format.json { render :json => entry}
          end
        else
          respond_to do |format|
            format.xml { render :xml => nil}
            format.json { render :json => nil}
          end
        end      
      else
        respond_to do |format|
          format.xml { render :xml => nil}
          format.json { render :json => nil}
        end
      end
    end    
  end  

  def delete    
    if params[:id]
      entry = Entry.find(params[:id])
      if can? :manage, entry      
        if entry.delete
          respond_to do |format|
            format.xml { render :xml => entry}
            format.json { render :json => entry}
          end
        else
          respond_to do |format|
            format.xml { render :xml => nil}
            format.json { render :json => nil}
          end
        end      
      else
        respond_to do |format|
          format.xml { render :xml => nil}
          format.json { render :json => nil}
        end
      end
    end    
  end  

  def download_image    
    image = Image.find(params[:id])
    path = File.expand_path("./public" + image.file.url(:original,false))
    respond_to do |format|
      format.download {send_file(path)}
    end
  end  

  def upload_image
    if params[:id]
      image = Image.find(params[:id])
    else
      image = Image.create(entry_id: params[:image][:entry_id])
    end
    
    if image
      pp "IMAGE FILE"
      pp params[:image][:file]
      image.file = params[:image][:file]
      if image.save
        entry = Entry.find(image.entry_id)
        entry.update_attributes(selected_image_id: image.id)
        # entry.complete_editing
        redirect_to entry_editing_path, :notice => 'Saved'
      end
    end
  end  
end
