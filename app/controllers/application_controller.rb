class ApplicationController < ActionController::Base
  protect_from_forgery

  # in ApplicationController
  def current_ability
    @current_ability ||= Ability.new(current_admin_user)
  end

  def after_sign_in_path_for(resource)
    # NOTE: couldn't get route :alias to work, 
    # ended up with conditional to check for photographer or editor roles and redirect if
          
    if current_admin_user.role == 'photographer'
      :entry_photography
    elsif current_admin_user.role == 'editor'
      :entry_editing
    end
  end

  # /* ***********************************************
  #   NEED TO SUBCLASS AdminUser for Photographer and then in the function below typecast the current admin user with that role.
  # *********************************************** */
  # def current_admin_user
  #   user = super
  #   if user.role == 'photographer'
  #     user.becomes(Photographer)
  #   end
  # end

  # /* ***********************************************
  #   REDIRECT THE LOGGED IN USER TO THE CORRECT PAGE AFTER LOGIN
  # *********************************************** */
  # def after_sign_in_path_for(resource_or_scope)
  #   if current_admin_user.role == 'photographer'
  #     celeb = current_admin_user.becomes(Celebrity)
  #     chatroom = celeb.chatrooms.last
  #     "/admin/chatrooms/#{chatroom.id}/join"
  #   end
  # end

end
