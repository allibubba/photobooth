require 'open-uri'
require 'net/http'

class UserController < ApplicationController
  def register
    # AN EXAMPLE OF APPLICATION CONFIGURATION - THIS IS SET IN THE config/environments/development.rb file
    # pp "/* ***********************************************"
    #   pp Rails.configuration.current_event_id
    # pp "*********************************************** */"
    if params[:user]
      @user = User.find_or_create_by_email_address(params[:user][:email_address])
      @user.update_attributes(params[:user])
      if @user && @user.persisted?
        @entry = @user.entries.create(:status => ENTRY_STATUS_REGISTERED, :event => Event.first)
        PrivatePub.publish_to("/queue/photo", {:entry_id => @entry.id, :user => @user, :entry => @entry, :action => 'create'})
        
        # /* ***********************************************
        # NOW THAT THE USER HAS REGISTERED AND AN ENTRY HAS BEEN CREATED - WE SHOULD BE PUSHING A NOTIFICATION TO THE NOTIFICATION CENTER
        # *********************************************** */
        redirect_to(root_path, :notice => 'User has been registered')
      else
        unless @user.errors.empty?
          flash.now[:notice] = "There are errors in the form. Fix the errors and try again."
        end
          
        render :action => 'new'        
      end
    else
      @user = User.new
      render :action => 'new'
    end
  end
  
  def generate_nickname
    nickname = get_nickname(params[:fullname])
    render :text => nickname
  end
  
  def get_nickname(fullname)
    begin
      url = URI.parse('http://www.mess.be/inickgenwuname.php')
      resp,data = Net::HTTP.post_form(url,{realname: fullname})
      results = resp.body.slice(resp.body.index('<font size=2>') + 13, resp.body.length)
      nickname = results.slice(0,results.index('</b>')).gsub!("\n","")
    rescue
      nickname = "no suggestion available"
    end
  end
  helper_method :get_nickname
end
