#ActiveAdmin::Dashboards.build do
ActiveAdmin.register_page "Dashboard" do

    controller do
        def index
            
            if current_admin_user.role == 'photographer'
              redirect_to :entry_photography
            elsif current_admin_user.role == 'editor'
              redirect_to :entry_editing
            end            
        end
    end    

end    

#   section "Recent Users" do
#     ul do
#       entries = Entry.limit(10)
#       path = root_path

#       entries.collect do |entry|
#         li link_to(entry.user.name, path)
#       end
#     end
#   end
# end