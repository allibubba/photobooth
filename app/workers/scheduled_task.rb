require 'timeout'
require 'socket'
def ping(host)
  begin
    Timeout.timeout(5) do 
      s = TCPSocket.new(host, 'echo')
      s.close
      return true
    end
  rescue Errno::ECONNREFUSED
    return true
  rescue Errno::EHOSTUNREACH
    return false
  rescue Timeout::Error
    return false
  end
end

class ScheduledTask
  @queue = "connectivity_check_#{Rails.env}"
  def self.perform()
    if ping('198.61.209.65')
      pp "----- CONNECTED --- RESET #{Resque::Failure.count} ENTRIES -------------------------------------------- "
      if Resque::Failure.count > 0
        (Resque::Failure.count-1).downto(0).each { |i| Resque::Failure.requeue(i) }
        Resque::Failure.clear
      end
    else
      pp "----- NOT CONNECTED #{Resque::Failure.count} ENTRIES WAITING -------------------------------------------- "
    end
  end
end