require 'timeout'
require 'socket'
def ping(host)
  begin
    Timeout.timeout(5) do 
      s = TCPSocket.new(host, 'echo')
      s.close
      return true
    end
  rescue Errno::ECONNREFUSED
    return true
  rescue Timeout::Error
    return false
  end
end

class EntryUploader
  @queue = "uploader_queue_#{Rails.env}"
  
  
  def self.perform(entry_id)
    if ping('198.61.209.65')
      entry = Entry.find(entry_id)
      user = entry.user
      selected_image = entry.selected_image

      user_params = user.attributes.delete_if {|key,valy| key == 'type' || key == 'id'}
      image_params = selected_image.attributes.delete_if {|key,value| key == 'type' || key == 'id' || key == 'entry_id'}
      entry_params = entry.attributes.delete_if {|key,value| key == 'type' || key == 'id' || key == 'user_id' || key == 'selected_image_id'}
      
      pp "* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * CONNECTING TO THE CLOUD * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "    
      cloud_user = CloudUser.find_or_create_by_email_address(user.email_address)
      cloud_user.update_attributes(user_params)
      cloud_entry = CloudEntry.create(entry_params)
      
      pp "* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * UPLOADING TO THE CLOUD * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "
      cloud_entry.selected_image = CloudImage.create(image_params)
      cloud_entry.save
      cloud_user.entries << cloud_entry
      
      saved = cloud_user.save!
      pp cloud_user
      pp saved
      if saved
        pp "* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * SYNCED TO THE CLOUD * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "
        entry.update_attributes(uploaded: true)
      end
    else
      raise "No Connection Found -- Will requeue and try again"
    end
  end
end