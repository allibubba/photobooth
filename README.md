# PhotoBooth App

### Notes  & warnings 
* Event management - Right now there is an Event model and the app can create Events in the admin. This is for future functionality. Only one event can be running per application instance. The first event in the database will be the event where all entries are assigned.
* This is running Faye server gem called PrivatePub for push notifications. In future versions this could be replaced using ActionController Live http://edgeapi.rubyonrails.org/classes/ActionController/Live.html I think this should reduce the need for thirdparty push notifications.
* There is code in the Entry Controller that needs to be refactored. There are several actions that are clear duplicates with only minor differences. It can be optimized for easier maintenance.
* **Constants** are used throughout the application to set statuses and to maintain the push notification channels. The constants can be found in the app `/config/initializers/constant.rb` it is auto loaded and the constants are available globally. [Deprecating the use of these constants] Especially in the use of the push channels. I have already started moving away from them in most situations.
* Rake tasks for clearing and populating stub data. You can run `rake users:populate` to add 20 users to the app, it adds them right to the photography queue and will be visible when the page is loaded. Also `rake users:destroy_all` will get rid of all the users and the entries in the app.


### Things that need to be taken care of
* Admin User authentication. I would like to be able to use the active_admin users to create some authentication for the editing queue and the photography queue. We need to limit access to these pages based on login credentials. This will also allow us to assign an owner to the entry while it is being edited.
* Entry file management - A decision needs to be made about how and when to create and organize files and folders that are associated to entries or users. 

- start photo (call clear tmp/photos dir script)
- take pictures with camera to to tmp/photos directory
- stop photo (does nothing / call clear tmp/photos dir script)
- complete photo -> moves tmp/* to perm location (/public/uploads/entries/)
- create image object from each file in /public/uploads/entries/ (relate entry to file)
-- images.create();



* Also the image manipulation - Thumbnail creation and image resizing after the user photography session. This all needs to get sorted out.
* Moderation - I would like to add a moderation view and add the ability flag an entry and have it pulled of the site.
* Implement the delete functionality on the front end. We should only be doing a soft delete. All entries will remain in the database, the status will be changed to `ENTRY_STATUS_DELETED`
* The front end needs to be sorted out … Porting over the front end from the padrino app will likely be the easiest thing to do.
* In the Edit Entries Queue - We should display a list (or thumbnails) of the images associated to the entry
	* Would be cool to open a selected image or an array of images in photoshop for editing
