# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
AdminUser.create(email: "edit@roundhouseagency.com", password: "4u2edit", role: "editor")
AdminUser.create(email: "photo@roundhouseagency.com", password: "4u2shoot", role: "photographer")
AdminUser.create(email: "dev@roundhouseagency.com", password: "4rhdev2use", role: "developer")