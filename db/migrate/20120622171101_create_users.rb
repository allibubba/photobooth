class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :name_first
      t.string :name_last
      t.string :name_nick
      t.string :hometown
      t.string :email_address

      t.timestamps
    end
  end
end
