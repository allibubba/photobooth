class AddPhotographerAndEditorToEntry < ActiveRecord::Migration
  def change
    add_column :entries, :photographer_id, :integer
    add_column :entries, :editor_id, :integer
  end
end
