class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.datetime :start_date
      t.string :location
      t.string :client

      t.timestamps
    end
  end
end
