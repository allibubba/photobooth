class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :local_path
      t.string :remote_path
      t.integer :entry_id

      t.timestamps
    end
  end
end
