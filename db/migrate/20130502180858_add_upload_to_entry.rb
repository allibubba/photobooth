class AddUploadToEntry < ActiveRecord::Migration
  def self.up
    add_column :entries, :uploaded, :boolean
  end

  def self.down
    remove_column :entries, :uploaded
  end
end