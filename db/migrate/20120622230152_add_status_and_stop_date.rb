class AddStatusAndStopDate < ActiveRecord::Migration
  def up
    add_column :events, :stop_date, :datetime
    add_column :events, :status, :integer
  end

  def down
    remove_column :events, :stop_date
    remove_column :events, :status
  end
end