class AddFileNameToImages < ActiveRecord::Migration
  def change
    add_column :images, :filename, :string
    remove_column :images, :local_path
  end
end
