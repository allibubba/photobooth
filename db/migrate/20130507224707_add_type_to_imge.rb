class AddTypeToImge < ActiveRecord::Migration
  def self.up
    add_column :images, :type, :string
  end

  def self.down
    remove_column :images, :type
  end
end