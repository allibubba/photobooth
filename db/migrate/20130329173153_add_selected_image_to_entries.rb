class AddSelectedImageToEntries < ActiveRecord::Migration
  def self.up
    add_column :entries, :selected_image_id, :integer
  end

  def self.down
    remove_column :entries, :selected_image_id
  end
end