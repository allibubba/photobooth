class AddAttendanceTaglineToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :attendance, :integer
    add_column :users, :tagline, :string
  end

  def self.down
    remove_column :users, :tagline
    remove_column :users, :attendance
  end
end