class CreateLifestyles < ActiveRecord::Migration
  def change
    create_table :lifestyles do |t|
      t.string :caption
      t.string :filename
      t.timestamps
    end
  end
end
