class AddImagePathsToImage < ActiveRecord::Migration
  def self.up
    add_column :images, :thumb, :string
    add_column :images, :medium, :string
    add_column :images, :large, :string
    add_column :images, :original, :string
  end

  def self.down
    remove_column :images, :original
    remove_column :images, :large
    remove_column :images, :medium
    remove_column :images, :thumb
  end
end