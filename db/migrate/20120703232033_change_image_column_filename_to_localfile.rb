class ChangeImageColumnFilenameToLocalfile < ActiveRecord::Migration
  def change
    add_column :images, :local_file, :string
    remove_column :images, :filename
  end
end
