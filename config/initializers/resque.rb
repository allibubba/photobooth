# This will make the tabs show up.
require 'resque_scheduler'
require 'resque_scheduler/server'

$resque_redis_config = { host: 'localhost', port: REDIS_PORT}

Resque.redis = Redis.new $resque_redis_config
Resque.schedule = YAML.load_file(Rails.root.join('config').to_s + '/resque_schedule.yml')


Resque::Server.use(Rack::Auth::Basic) do |user, password|
  password == "SeeMyQueue"
end
