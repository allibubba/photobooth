# /* ***********************************************
# USER STATUSES  
# *********************************************** */
ENTRY_STATUS_REGISTERED = 0

ENTRY_STATUS_PHOTOGRAPHY_ACTIVE = 1
ENTRY_STATUS_PHOTOGRAPHY_INACTIVE = 2
ENTRY_STATUS_PHOTOGRAPHY_COMPLETE = 3

ENTRY_STATUS_IMAGE_UPLOADED = 4
ENTRY_STATUS_FLAGGED_FOR_MODERATION = 5
ENTRY_STATUS_DELETED = 6

ENTRY_STATUS_EDITING_ACTIVE = 7
ENTRY_STATUS_EDITING_INACTIVE = 8
ENTRY_STATUS_EDITING_COMPLETE = 9

ENTRY_STATUS_COMPLETED = 10


# /* ***********************************************
#   EVENT STATUSES
# *********************************************** */
EVENT_STATUS_CREATED = 0
EVENT_STATUS_STARTED = 1
EVENT_STATUS_STOPPED = 2


# /* ***********************************************
#   PUSH NOTIFICATION CHANNELS
# *********************************************** */
PUSH_CHANNEL_ENTRY_CREATED = '/entry/created'
PUSH_CHANNEL_USER_REGISTER = '/user/registered'

PUSH_CHANNEL_ENTRY_PHOTO_START = '/entry/photo/started'
PUSH_CHANNEL_ENTRY_PHOTO_STOP = '/entry/photo/stopped'
PUSH_CHANNEL_ENTRY_PHOTO_COMPLETE = '/entry/photo/completed'
  
PUSH_CHANNEL_ENTRY_EDIT_START = '/entry/edit/started'
PUSH_CHANNEL_ENTRY_EDIT_STOP = '/entry/edit/stopped'
PUSH_CHANNEL_ENTRY_EDIT_COMPLETE = '/entry/edit/completed'
  

# /* ***********************************************
#   PATH SETTINGS
# *********************************************** */  
PATH_IMAGES_RAW           = './tmp/photos/'
PATH_IMAGES_APPLICATION   = './public/uploads/'
PATH_IMAGES_REMOTE        = 'http://google.com/'
