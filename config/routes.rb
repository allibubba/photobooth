PhotoBooth::Application.routes.draw do
  root :to => 'User#register'

  mount Resque::Server, :at => "/resque"

  match 'register' => 'User#register', :as => 'register'
  match 'photography' => 'Entry#photography', :as => 'entry_photography'
  match 'editing' => 'Entry#editing', :as => 'entry_editing'
  match 'completed' => 'Entry#completed', :as => 'completed_entries'
  match 'last' => 'Entry#last', :as => 'last_entry'
  get 'generate-nickname(/:fullname)' => 'User#generate_nickname', :as => :nickname
  
  resources :image do
    member do
      get 'open' => 'Entry#open_image'
    end
  end

  #resources :download, :only => [:image]

  resources :entry do
    member do
      get 'photography_start'
      get 'photography_stop'
      get 'photography_completed'
        
      get 'editing_start'
      get 'editing_stop'
      get 'editing_completed'
      
      get 'flag'
      get 'delete'
    end
  end

  match 'image/download/:id'  => 'Entry#download_image',  :as => 'download_image'
  match 'image/upload(/:id)'    => 'Entry#upload_image',    :as => 'upload_image'

  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config
end
